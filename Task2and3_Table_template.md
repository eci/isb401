# Table Templates for Task 2 & 3

These are table templates for Tasks 2 & 3 of Practical 5 of MISB401. 
Interactive notepad sessions can be created at https://hpc.uni.lu/pad/

##  Task 2 Table

|Name         |ID|Code|Formula  |[M+H]+|RT (min)|MS2?|Num. Struct|Level|Initials|
|-------------|--|----|---------|------|--------|----|-----------|-----|--------|
|Glycine      | 1|Gly|C2H5NO2   | 76.0393|12.61|No|1|5*|ES|
|Serine       | 2|Ser|C3H7NO3   |106.0499|12.61|Yes|2(1)|1|ES|
|Valine       | 3|Val|C5H11NO2  |118.0863| | | | |XX|
|Threonine    | 4|Thr|C4H9NO3   |120.0655| | | | |XX|
|Leucine      | 5|Leu|C6H13NO2  |132.1019| | | | |XX|
|Asparagine   | 6|Asn|C4H8N2O3  |133.0608| | | | |XX|
|Glutamine    | 7|Gln|C5H10N2O3 |147.0764| | | | |XX|
|Lysine       | 8|Lys|C6H14N2O2 |147.1128| | | | |XX|
|Histidine    | 9|His|C6H9N3O2  |156.0768| | | | |XX|
|Arginine     |10|Arg|C6H14N4O2 |175.1190| | | | |XX|
|Tryptophan   |11|Trp|C11H12N2O2|205.0972| | | | |XX|
|Cysteine     |12|Cys|C3H7NO2S  |122.0270| | | | |XX|
|Aspartate    |13|Asp|C4H7NO4   |134.0449| | | | |XX|
|Glutamate    |14|Glu|C5H9NO4   |148.0604| | | | |XX|
|Phenylalanine|15|Phe|C9H11NO2  |166.0863| | | | |XX|
|Tyrosine     |16|Tyr|C9H11NO3  |182.0812| | | | |XX|
|Alanine      |17|Ala|C3H7NO2   |90.05495| | | | |XX|
|Proline      |18|Pro|C5H9NO2   |116.0706| | | | |XX|
|Isoleucine   |19|Ile|C6H13NO2  |132.1019| | | | |XX|
|Methionine   |20|Met|C5H11NO2S |150.0583| | | | |XX|


## Task 3 Table 

|Name|m/z [M+H]+| WT:RT (min)|WT:Int.|WT:MS/MS|KO:RT [min]|KO:Int.|KO:MS/MS|Ratio WT:KO|Init.|
|----|----------|------------|-------|--------|-----------|-------|--------|-----------|-----|
|Serine|106.0499|12.58|1x10^6|No|12.59|1x10^6|No|1|ES|
| | | | | | | | | |XX|
| | | | | | | | | |XX|
| | | | | | | | | |XX|
| | | | | | | | | |XX|
