# Using Shinyscreen for ISB401

## Step 1: Project Tab

### 1. Project management tab
	1. `Select project`: Select isb401.
	2. Click `Load/Initialise`.
	3. `Input directories`: Select isb401.
	4. Click `Select`.
	5. Confirm that both `Current project` and `Current data directory` show **isb401**.
	
### 2. Compound list inputs tab
	1. `Select compound lists`: Select `AAs.csv`.
	2. Click upper `Select`.
	3. `Select set lists`: Select `AA_set.csv`.
	4. Click lower `Select`.
	5. Confirm that `selected compounds` and `selected setid lists`
       show the selections.
	   
### 3. Data files
	1. `Select datafiles`: Select all three.
	2. In the table below, change tags if you will.
	3. In the last table (the one below the step 2. table), select appropriate adducts and sets.

## Step 2: Configure, Extract, Prescreen Tab
	1. Adjust extraction/prescreen parameters (if necessary).
	2. Click `Extract`.
	3. Click `Prescreen`.
	
## Step 3: Results Explorer
	1. Click on any `Compound Index` entry to display plots. 
	   - If you get red errors in the corner, this is the structure plot not being debugged yet.
	   - If there are no other plots, reload the page in the browser, then click again.


